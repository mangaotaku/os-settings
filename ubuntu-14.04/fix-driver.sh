cd /lib/modules/4.2.0-38-generic/updates/dkms/
mkdir old
mv nvidia_361* old/
ln -s nvidia.ko nvidia_361.ko
ln -s nvidia-modeset.ko nvidia_361_modeset.ko
ln -s nvidia-modeset.ko nvidia_361-modeset.ko
ln -s nvidia-drm.ko nvidia_361_drm.ko
ln -s nvidia-uvm.ko nvidia_361_uvm.ko