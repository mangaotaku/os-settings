git clone https://github.com/Genoil/cpp-ethereum/
cd cpp-ethereum/
git checkout 110
mkdir build
cd build
cmake -DBUNDLE=cudaminer -DCOMPUTE=61 ..
make -j32
make install
cd /usr/local/lib/
mv lib* /usr/lib/